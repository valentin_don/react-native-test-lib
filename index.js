import React from 'react';
import { NativeModules, requireNativeComponent } from 'react-native';

const AlertLib  = requireNativeComponent('RNTCustomAlert');

export default class CustomAlertView extends React.Component {
    render() {
      return ( <AlertLib/>)
    }
  }
