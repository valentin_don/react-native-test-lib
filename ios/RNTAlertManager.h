//
//  RNTAlertManager.h
//  TestLib
//
//  Created by vdonloc on 16.12.2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <React/RCTViewManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface RNTAlertManager : RCTViewManager

@end

NS_ASSUME_NONNULL_END
