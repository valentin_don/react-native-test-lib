//
//  RNTAlertManager.m
//  TestLib
//
//  Created by vdonloc on 16.12.2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "RNTAlertManager.h"
#import "TestView.h"

@implementation RNTAlertManager
RCT_EXPORT_MODULE(RNTCustomAlert)
RCT_EXPORT_VIEW_PROPERTY(isRed, BOOL)
- (UIView *)view
{
  return [[TestView alloc] init];
}
@end
