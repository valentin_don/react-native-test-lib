//
//  TestView.m
//  TestLib
//
//  Created by vdonloc on 16.12.2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "TestView.h"
@interface TestView()
@property (nonatomic, strong) UIButton * button;
@end
@implementation TestView
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setup {
    UIButton * btn  = [[UIButton alloc]initWithFrame:CGRectMake(20, 20, 200, 50)];
    [btn setTitle:@"Test Button" forState:UIControlStateNormal];
    [btn setTitleColor:UIColor.darkTextColor forState:UIControlStateNormal];
    [btn setBackgroundColor:UIColor.greenColor];
    [btn addTarget:self action:@selector(buttonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
    
    self.frame = CGRectMake(0, 0, 300, 100);
    self.backgroundColor = UIColor.redColor;
}

- (void) buttonTap:(id) sender {
//    UIAlertController *vc = [UIAlertController alertControllerWithTitle:@"Alert Lib" message:@"Test from lib" preferredStyle:UIAlertControllerStyleAlert];
    
    
}
@end
